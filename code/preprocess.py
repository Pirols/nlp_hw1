from pathlib import Path

def getPaths(labelsName, inputName):

    i = 0
    lName = labelsName + ".utf8"
    iName = inputName + ".utf8"

    while Path(lName).is_file() or Path(iName).is_file():
        lName = labelsName + str(i) + ".utf8"
        iName = inputName + str(i) + ".utf8"
        i += 1

    return lName, iName

def preprocess(file):
    """
    This function will create 2 files:
        - A labels file, which will contain a label for each character in the passed file
            Labels are assigned according to the following rules:
            - Spaces(of any kind) are ignored
            - S -> Single character
            - B -> First character of a word
            - I -> Internal character of a word
            - E -> Last character of a word
        - An input file, which will contain the text of the passed file without any space between characters
    Parameters:
        - file, should contain the path to the file which will be parsed
    This function returns a tuple containing:
        - labelsPath: the path to the labels file
        - inputPath: the path to the input file
        Note: If the parameter file is a relative path to the file to be parsed the return values will be relative paths as well
    """

    # if possible it reads the passed file
    try:
        with open(file=file, mode="r", encoding="utf-8") as f:
            lines = f.readlines()
    
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c = type(e).__name__, m = str(e)))
        return -1

    # extrapolates the directory the passed file is in and its name
    directory = file[0:file.rfind(".")]

    # checks if there already exists a file with the same path of the labels ( directory-labels.utf8 ) or the input ( directory-input.utf8 ) files we want to create 
    # if yes it changes the paths in directory-labelsI.utf8 and directory-inputI.utf8 where I is the first integer, starting from 0, such that:
    # there are no directory-labelsI.utf8 nor directory-inputI.utf8 files
    # otherwise the paths will remain directory-labels.utf8 and directory-input.utf8
    labels_path, input_path = getPaths(directory + "-labels", directory + "-input")

    # creates the 2 files
    labels = open(file=labels_path, mode="w", encoding="utf-8")
    inp = open(file=input_path, mode="w", encoding="utf-8")

    for line in lines:
        words = line.split()

        for word in words:
            length = len(word)

            # if the word is composed of just one character that must be labelled with "S" 
            # Otherwise the word must be labelled with:
                # one "B" for the first character
                # one "I" for each internal character
                # one "E" for the last character
            #    
            # writes the appropriate label to the labels.utf8 file
            labels.write("S" if length == 1 else "B" + "I"*(length-2) + "E")

            # writes every word to the input.utf8 file without any spaces in-between words.
            inp.write(word)

        # adds end of lines in the files when the actual line finishes
        inp.write("\n")
        labels.write("\n")

    # closing the opened files to avoid issues
    inp.close()
    labels.close()

    return labels_path, input_path

if __name__ == "__main__":
    """
        I've used this code to preprocess all the datasets in the training folder
    """
    # NOTICE: This path is relative 
    #datasetsPath = "datasets/"

    #preprocess(datasetsPath + "training/cityu_training.utf8")
    #preprocess(datasetsPath + "training/as_training.utf8")
    #preprocess(datasetsPath + "training/msr_training.utf8")
    #preprocess(datasetsPath + "training/pku_training.utf8")

    #preprocess(datasetsPath + "development/cityu_test_gold.utf8")
    #preprocess(datasetsPath + "development/as_testing_gold.utf8")
    #preprocess(datasetsPath + "development/msr_test_gold.utf8")
    #preprocess(datasetsPath + "development/pku_test_gold.utf8")
