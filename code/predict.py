from argparse import ArgumentParser
import tensorflow as tf
from tensorflow import keras as K
import numpy as np

from train import indexVocab
from tsv2voc import csv2voc

def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_path", help="The path of the input file")
    parser.add_argument("output_path", help="The path of the output file")
    parser.add_argument("resources_path", help="The path of the resources needed to load your model")

    return parser.parse_args()

def splitList(input_path):
    """
    This function is used to create 2 lists:
        splitted_lines, which containes all the lines in the input file but divided into, shorter than 128, sentences; to avoid issues during the predicting phase
        lengths, which containes the original lengths of the lines of the input file; I will use this to re-join the sentences in the right way after the prediction
    """

    with open(input_path, mode="r", encoding="utf-8") as x:
        lines = x.readlines()

    lengths = list()
    splitted_lines = list()
    for line in lines:
        # Store the lengths of every line without the "\n" character
        lengths.append(len(line)-1)
        
        # Splits a sequence longer than 128 into smaller once, all long 128 apart from the last one
        while len(line) > 128:
            splitted_lines.append(line[:128])
            line = line[128:]

        splitted_lines.append(line)
    
    return splitted_lines, lengths

def setupData(lines, uni2index, bi2index):
    """
    This function just takes care of preparing the input_file to be parsed by the network, it one list containing the 2 following lists:
        unigrams, which containes the indices of the unigrams in lines. Notice the indices are determined by the vocabulary uni2index
        bigrams, which containes the indices of the bigrams in lines. Notice the indices are determined by the vocabulary bi2index
    """
    unigrams = list()
    bigrams = list()
    for line in lines:
        # I create 2 lists per sentence, one containing the indices associated to the unigrams, the other those associated to the bigrams
        uni_arr = list()
        bi_arr = list()
        for i in range(len(line)):
            if line[i] == "\n":
                # I add the padding to make every list of unigrams and bigrams 128 integers long
                uni_arr.extend([0]*(128-len(uni_arr)))
                bi_arr.extend([0]*(128-len(bi_arr)))
                continue
                
            # gathers and appends the right index for the unigram
            if line[i] in uni2index:
                uni_arr.append(uni2index[line[i]])
            else:
                uni_arr.append(uni2index["<UNK>"])

            # gathers and appends the right index for the bigram
            if line[i:i+2] in bi2index:
                bi_arr.append(bi2index[line[i:i+2]])
            else:
                bi_arr.append(bi2index["<UNK>"])

        unigrams.append(uni_arr)
        bigrams.append(bi_arr)

    return [unigrams, bigrams]

def convertToLabels(predictions, lengths):
    """
        This function takes as parameter the lengths list, return by the splitList function, and the predictions obtained running predict on the model,
        and it returns a string containing the translation of the predictions into actual labels, also re-ordering them into the original lines,
        whose lengths are stored in lengths.
    """
    s = ""

    iterator = iter(predictions)

    for length in lengths:
        
        while length > 128:
            for el in next(iterator):
        
                i = np.argmax(np.array(el))
                if i == 0:
                    s += "B"
                elif i == 1:
                    s += "I"
                elif i == 2:
                    s += "E"
                else:
                    s += "S"
            length -= 128
        for el in next(iterator)[:length]:
            i = np.argmax(np.array(el))
            if i == 0:
                s += "B"
            elif i == 1:
                s += "I"
            elif i == 2:
                s += "E"
            else:
                s += "S"
        s += "\n"
            
    return s


def predict(input_path, output_path, resources_path):
    """
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the BIES format.
    
    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.
    
    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.

    :param input_path: the path of the input file to predict.
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    # load the model and its weights
    model = K.models.load_model(resources_path + "/my_model.h5")
    model.load_weights(resources_path + "/my_model_weights.h5")

    # split lines to handle sentences longer than 128 characters
    splitted_lines, lengths = splitList(input_path)

    uni2vec = csv2voc(resources_path + "/embeddings/embeddings.csv", 1)
    bi2vec = csv2voc(resources_path + "/embeddings/embeddings.csv", 2)

    # I need these dictionaries to map unigrams and bigrams to integers which will be fed into the model
    uni2index = indexVocab(uni2vec)
    bi2index = indexVocab(bi2vec)
            
    # Basically I map unigrams and bigrams to indices and create one array that can be fed into the model
    inputs = setupData(splitted_lines, uni2index, bi2index)
    
    # predicts the labels
    predictions = model.predict(inputs)

    # Converts from arrays of probabilities into actual labels
    s = convertToLabels(predictions, lengths)

    
    with open(output_path, mode="w") as f:
        f.write(s)

if __name__ == '__main__':
    args = parse_args()
    predict(args.input_path, args.output_path, args.resources_path)
