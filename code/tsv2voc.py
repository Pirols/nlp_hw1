import numpy as np

def tsv2csv(file):
    """ 
        This function is used to create a csv file equivalent to zh.tsv
        zh.tsv contains the pretrained embeddings for the Chinese language, available here: https://github.com/Kyubyong/wordvectors 
        It features 1,2,3 and 4-grams each of which is associated to a vector of 300 elements.
    """
    
    # path to the csv file
    path = file[0:file.rfind("/") + 1] + "embeddings.csv"

    # reads and splits the input file
    with open(file, mode="r", encoding="utf-8") as f:
        elements = f.read().split()

    # creates the output file
    fd = open(path, mode="w",encoding="utf-8")

    # I use the s variable to briefly store the string to write to the csv file
    s = ""

    for el in elements:
        # The only characters in zh.tsv which are digits are the indices associated to the characters
        # Also we don't need to store the "[" 
        if el.isdigit() or el == "[":
            continue

        # Means we are the end of the 300-vector of the actual character, therefore we write the actual line and reset the s variable for the next character
        # Also I need to delete the last character because it is a comma
        if el == "]":
            fd.write(s[:-1] + "\n")
            s = ""
            continue
            
        # sometimes the "[" is directly connected with the first float of the 300-vector of a character, let's just discard the bracket
        if(el[0] == "["):
            s += el[1:] + ","

        # sometimes the "]" is directly connected with the last float of the 300-vector of a character, let's just discard the bracket, then write to the file
        # and finally reset the s variable for the next character
        elif(el[-1] == "]"):
            fd.write(s + el[:-1] + "\n")
            s = ""

        # just writes simple values
        else:
            s += el + ","

    # I also add a <UNK> key and a random 300-vector as its value
    vec = np.random.uniform(low=-1.0, high=1.0, size=(300))
    fd.write("<UNK>,")
    s = ""
    for el in vec:
        s += str(el) + ","

    fd.write(s[:-1] + "\n")

    # closes the file descriptor to embeddings.csv to avoid issues     
    fd.close()

    return path

def csv2voc(file, n = 1):
    """
        This function takes as input the path to a csv file and returns a dictionary, according to the following rules:
            - The keys are the first elements of every line
            - The values are lists formed with all the elements of the line except the first one
            - The returned dictionary will contain only the n-grams where n is an optional parameter, default is 1.
            - If multiple occurrences of the same key are found an error message is printed and the function ends returning -1
    """

    # reads the input file
    with open(file, mode="r", encoding="utf-8") as f:
        text = f.read()

    lines = text.splitlines()

    voc = {}

    for line in lines:
        # We always want the <UNK> key and its value
        if line[0:line.find(",")] == "<UNK>":
            # line.find(",") must be equal to 5
            voc[line[0:5]] = []

            arr = line[5+1:].split(",")
            for val in arr:
                voc[line[0:5]].append(float(val))

        # Parsing only file whose first comma is found at index n we will consider only n-grams in our vocabulary
        if line.find(",") != n:
            continue

        # If a key is already in the dictionary an error message is printed and the function ends returning -1
        if line[0:n] in voc:
            print("The file: " + str(file) + " contains multiple occurrences of this symbol: " + str(line[0:n]))
            return -1

        # creates a list for every different n-gram
        voc[line[0:n]] = []
        
        arr = line[n+1:].split(",")
        # it assigns the 300-embedding-vector to the respective n-gram 
        for val in arr:
            voc[line[0:n]].append(float(val))

    return voc

if __name__ == "__main__":
    """ 
        I've used this code to convert the zh.tsv file in an easier to read .csv file
    """
    
    # Note this is a relative Path
    #embeddingsPath = "resources/embeddings/"

    #csvPath = tsv2csv(embeddingsPath + "zh.tsv")