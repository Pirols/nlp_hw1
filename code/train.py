import tensorflow as tf
from tensorflow import keras as K
import numpy as np
import h5py

from tsv2voc import csv2voc

def indexVocab(voc):
    # Returns the vocab which maps all the keys (and "<PAD>" which I want to be mapped to 0) of the parameter voc to unique integers
    indexVocab = dict()

    i = 1
    for key in voc:
        indexVocab[key] = i
        i += 1
    
    indexVocab["<PAD>"] = 0

    return indexVocab

def setup_embeddings_matrix(voc, indexVocab, embedding_size):
    # Returns the embeddings matrix which is composed by the embeddings of a token stored in i-th row where the token is mapped to i in the indexVocab

    embeddings_matrix = np.zeros((len(indexVocab), embedding_size))

    for token, index in indexVocab.items():
        if token in voc:
            embeddings_matrix[index] = voc[token]

    return embeddings_matrix


def create_model(uni_vocab_size, bi_vocab_size, uni_embeddings_matrix, 
                 bi_embeddings_matrix, uni_embedding_size, bi_embedding_size, 
                 hidden_size, dropout, recurrent_dropout, lr, decay, verbose=False):
    """ 
        This function creates and returns a keras model with 2 inputs each fed to an embedding layer with pretrained embeddings,
        which then get concatenated and passed to a Bidirectional LSTM which then outputs to a final Dense Layer, 
        these last 2 layers are those whose weights will be trained.
    """

    # I define one Input layer for the unigrams and one for the bigrams
    InputUni = K.layers.Input(shape=(128, ), name = "Unigrams")
    InputBi = K.layers.Input(shape=(128, ), name = "Bigrams")

    # I define one Embedding layer for the unigrams and one for the bigrams, both with pretrained embeddings, 
    # mask_zero is set to True because we want to ignore the padding in both unigrams and bigrams
    Uni_EMBD = K.layers.Embedding(uni_vocab_size, uni_embedding_size, weights=[uni_embeddings_matrix], trainable=False, mask_zero=True, name = "Uni_Embeddings")(InputUni)
    Bi_EMBD = K.layers.Embedding(bi_vocab_size, bi_embedding_size, weights=[bi_embeddings_matrix], trainable=False, mask_zero=True, name = "Bi_Embeddings")(InputBi)

    # I define one Concatenate layer which concatenates unigrams and bigrams
    Concat_EMBD = K.layers.Concatenate(name = "UniBi_Embedded")([Uni_EMBD, Bi_EMBD])

    # I define the Bidirectional LSTM which is the layer I will actually train
    BiLSTM = K.layers.Bidirectional(K.layers.LSTM(hidden_size, dropout=dropout, recurrent_dropout=recurrent_dropout, return_sequences=True))(Concat_EMBD)
    
    # I define one output layer
    Output = K.layers.Dense(4, activation="softmax", name = "Output")(BiLSTM)

    # I build the model joining all the previously defined layers
    model = tf.keras.Model(inputs=[InputUni, InputBi], outputs=Output)

    adam = K.optimizers.Adam(lr=lr, decay=decay)
    model.compile(loss="categorical_crossentropy", optimizer=adam, metrics=["categorical_accuracy"])

    if verbose:
        model.summary()

    return model

def setup_labels(y_path, verbose=False):
    B_EMBEDDING = [1, 0, 0, 0]
    I_EMBEDDING = [0, 1, 0, 0]
    E_EMBEDDING = [0, 0, 1, 0]
    S_EMBEDDING = [0, 0, 0, 1]
    PAD_EMBEDDING = [0, 0, 0, 0]

    with open(y_path, mode="r", encoding="utf-8") as y:
        lines = y.readlines()
        
    for line in lines:
        # I parse lines longer than 128 characters in order to avoid issues afterwards, since the network needs input with fixed length=128
        # NOTICE: Applying this cycle to both inputs and labels keeps the link between one input line and a label one
        #         Also I don't do like this in predict.py because THERE we care about ordering, whereas in the training process some shuffling might even help improving the accuracy
        if len(line)>128:
            lines.remove(line)
            lines.append(line[:128])
            lines.append(line[128:])

    labels = list()
    for line in lines:
        arr = list()
        for c in line:
            if c == "\n":
                # I add the padding to make every sentence 128 characters long
                arr.extend([PAD_EMBEDDING]*(128-len(arr)))
            # Let's convert the labels into their embeddings
            elif c == "B":
                arr.append(B_EMBEDDING)
            elif c == "I":
                arr.append(I_EMBEDDING)
            elif c == "E":
                arr.append(E_EMBEDDING)
            elif c == "S": 
                arr.append(S_EMBEDDING)
            else:
                print("Unknown labels: " + c)
                return -1
        # I append the 128 array to the return array        
        labels.append(arr)
        
    # Convert the return array into a np.array
    labels = np.array(labels)

    if verbose:
        print("Label Shape: ", np.shape(labels))

    return labels

def setup_input(x_path, uni2index, bi2index, verbose=False):

    with open(x_path, mode="r", encoding="utf-8") as x:
        lines = x.readlines()
    
    for line in lines:
        # I parse lines longer than 128 characters in order to avoid issues afterwards, since the network needs input with fixed length=128
        # NOTICE: Applying this cycle to both inputs and labels keeps the link between one input line and a label one
        if len(line)>128:
            lines.remove(line)
            lines.append(line[:128])
            lines.append(line[128:])
    
    unigrams = list()
    bigrams = list()

    for line in lines:
        # I create 2 lists per sentence, one containing the indices associated to the unigrams, the other those associated to the bigrams
        uni_arr = list()
        bi_arr = list()
        for i in range(len(line)):
            if line[i] == "\n":
                # I add the padding to make every list of unigrams and bigrams 128 integers long
                uni_arr.extend([0]*(128-len(uni_arr)))
                bi_arr.extend([0]*(128-len(bi_arr)))
                continue
                
            if line[i] in uni2index:
                uni_arr.append(uni2index[line[i]])
            else:
                uni_arr.append(uni2index["<UNK>"])

            if line[i:i+2] in bi2index:
                bi_arr.append(bi2index[line[i:i+2]])
            else:
                bi_arr.append(bi2index["<UNK>"])

        unigrams.append(uni_arr)
        bigrams.append(bi_arr)

    if verbose:
        print("Unigrams input shape: ", np.shape(unigrams))        
        print("Bigrams input shape: ", np.shape(bigrams))

    return [unigrams, bigrams]

def setup_datasets(train_x_path, train_y_path, dev_x_path, dev_y_path, uni2index, bi2index, verbose=False):

    train_x = setup_input(train_x_path, uni2index, bi2index, verbose=verbose)
    train_y = setup_labels(train_y_path, verbose=verbose)

    dev_x = setup_input(dev_x_path, uni2index, bi2index, verbose=verbose)
    dev_y = setup_labels(dev_y_path, verbose=verbose)

    return (train_x, train_y, dev_x, dev_y)

def train(batch_size, epochs, train_x_path, 
          train_y_path, dev_x_path, dev_y_path, 
          hidden_size, uni_embedding_size, bi_embedding_size, 
          dropout, recurrent_dropout, lr, decay, verbose=False):
    """
        This function builds the model and trains it
    """
    # Gathers the dictionaries which map unigrams and bigrams to a 300-vector
    uni2vec = csv2voc("resources/embeddings/embeddings.csv", 1)
    bi2vec = csv2voc("resources/embeddings/embeddings.csv", 2)

    # Creates the dictionaries which map unigrams and bigrams to some unique integer
    # NOTICE: there aren't 2 unigrams which map to the same integer, although unigrams and bigrams can map to the same integer
    #         I allowed this because having 2 embedding layers it doesn't make any difference 
    uni2index = indexVocab(uni2vec)
    bi2index = indexVocab(bi2vec)
    
    # I add to both the dictionaries the key "<PAD>" which maps to a 300-vector whose elements are all zeros.
    uni2vec["<PAD>"] = np.zeros((300))  
    bi2vec["<PAD>"] = np.zeros((300))

    # I create the matrices which will be needed to create the model
    uni_embeddings_matrix = setup_embeddings_matrix(uni2vec, uni2index, uni_embedding_size)
    bi_embeddings_matrix = setup_embeddings_matrix(bi2vec, bi2index, bi_embedding_size)
    
    if verbose:
        print("This is the model to train:")

    # creates the model
    model = create_model(
        uni_vocab_size=len(uni2vec), bi_vocab_size=len(bi2vec), 
        uni_embeddings_matrix=uni_embeddings_matrix, bi_embeddings_matrix=bi_embeddings_matrix, 
        uni_embedding_size=uni_embedding_size, bi_embedding_size=bi_embedding_size,
        hidden_size=hidden_size, dropout=dropout, recurrent_dropout=recurrent_dropout, 
        lr=lr, decay=decay, verbose=verbose
        )

    # saves the model which I will load in the predict function
    model.save('my_model.h5')

    (train_x, train_y, dev_x, dev_y) = setup_datasets(train_x_path, train_y_path, dev_x_path, dev_y_path, uni2index, bi2index)

    # Actual training part
    model.fit(x=[train_x[0][:200000], train_x[1][:200000]], 
                y=train_y[:200000], 
                batch_size=batch_size, epochs=epochs, 
                validation_data=([dev_x[0][:8000], dev_x[1][:8000]], dev_y[:8000]), 
                verbose=(1 if verbose else 0))

    # saves the weights I will load into the model in the predict function
    model.save_weights('my_model_weights.h5')

if __name__ == "__main__":
    """
        I trained on google colab and just pasted the code here to show how I did that
    """
    
    # All the hyperparameters are specified here
    HIDDEN_SIZE = 128

    # Notice: These embeddings are thought to be 300 and shouldn't be changed since I'm using pretrained embeddings
    UNI_EMBEDDING_SIZE = 300
    BI_EMBEDDING_SIZE = 300

    DROPOUT = 0.2
    RECURRENT_DROPOUT = 0.2

    LEARNING_RATE = 1e-4
    DECAY = 1e-5

    BATCH_SIZE = 64
    EPOCHS = 1

    train(
        batch_size=BATCH_SIZE, epochs=EPOCHS,
        train_x_path="datasets/training/as_training-input.utf8",
        train_y_path="datasets/training/as_training-labels.utf8",
        dev_x_path="datasets/development/as_testing_gold-input.utf8",
        dev_y_path="datasets/development/as_testing_gold-labels.utf8",
        hidden_size=HIDDEN_SIZE,
        uni_embedding_size=UNI_EMBEDDING_SIZE,
        bi_embedding_size=BI_EMBEDDING_SIZE,
        dropout=DROPOUT, 
        recurrent_dropout=RECURRENT_DROPOUT, 
        lr=LEARNING_RATE, 
        decay=DECAY,
        verbose=True
    )